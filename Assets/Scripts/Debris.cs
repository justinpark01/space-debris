﻿using UnityEngine;
using System.Collections;

public class Debris : MonoBehaviour 
{
    public float Size;
    public float X;
    public float Y;
    public float dX;
    public float dY;
    public float CreationTime;
    
    // Use this for initialization
	void Start () 
    {
        CreationTime = Time.time;
        
        bool FromLeft = Random.Range(0, 2) == 1;
        bool FromBottom = Random.Range(0, 2) == 1;
        bool LeftToRight = Random.Range(0, 2) == 1;
        Size = Random.RandomRange(.1f, .5f);
        this.transform.localScale = new Vector3(Size, Size);

        if (LeftToRight)
        {
            X = FromLeft ? -12f : 12f;
            Y = FromBottom ? Random.Range(-8f, 0f) : Random.Range(0f, 8f);
        }
        else
        {
            X = FromLeft ? Random.Range(-12f, 0f) : Random.Range(12f, 0f);
            Y = FromBottom ? -8f : 8f;
        }       

		this.transform.localPosition = new Vector3 (X, Y, 0);

        dX = (X < 0) ? Random.Range(.5f, 1.5f) : Random.Range(-.5f, -1.5f);
        dY = (Y < 0) ? Random.Range(.5f, 1.5f) : Random.Range(-.5f, -1.5f);
        AdjectDirectionTowardsISS();
	}

    private void AdjectDirectionTowardsISS()
    {

    }

	// Update is called once per frame
    void Update()
    {
        X += dX * Time.deltaTime;
        Y += dY * Time.deltaTime;
        this.transform.localPosition = new Vector3(X, Y, 0);

        if (Time.time > CreationTime + 60)
            Destroy(gameObject);
    }
}
