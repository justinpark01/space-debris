﻿using UnityEngine;
using System.Collections;

public class DebrisGenerator : MonoBehaviour 
{
    private float LastDebrisCreated;
    private float LastDifficultyIncrease;
    public float RegenerationRate = 3f;
    public float IncreaseDifficultyRate = 30f;

    void Start () 
    {
        LastDebrisCreated = Time.time;
        LastDifficultyIncrease = Time.time;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Time.time - LastDebrisCreated > RegenerationRate)
        {
            GameObject.Instantiate(Resources.Load("Debris"));
            LastDebrisCreated = Time.time;
        }

        if (Time.time - LastDifficultyIncrease > IncreaseDifficultyRate)
        {
            RegenerationRate -= .3f;
            LastDifficultyIncrease = Time.time;
        }
	}
}
